# Monte Carlo Simulation in Inventory Management

# Scenario

The Mission: Authorised by the UN, you will be sent to Mars to open a new flagship store for ‘Super
Apple’ for a duration of 52 weeks. You will be offered a very competitive basic salary, plus a
performance related bonus.

The Market: The ‘Super Apple’ is an expensive nutritional product. Its weekly demand on Mars is a
random number with the following probability distribution

Demand 		(D) 0 	 1    2    3   4    5    6

Probability (p) 0.04 0.10 0.26 0.4 0.16 0.02 0.02

Inventory Management: You are responsible for managing the store and your key job is to control the
inventory, including paying for warehouse costs, ordering from the earth etc. Due to transport limitations,
you will apply a fixed order policy, i.e. during these 52 weeks, you will order a fixed number 𝑦𝑦 units of
‘Super Apple’ from Earth once your stock is less than or equal to a fixed level of 𝑟 units.

Note: only integer number units of ‘Super Apple’ can be ordered.

Assumptions: (1) You will check your stock level at the end of each week (i.e. Friday at 17:00) and place
an order if necessary; (2) If you place an order, the ‘Super Apple’ will be delivered to you before 9:00 the
next Monday; (3) You start your job on the Friday before the 1st week with 0 initial stock.

Your Performance: The UN has decided to measure your performance based on your operational cost,
including:

1 Warehouse Cost: if at the end of a week you have any stock left, you will have to store them in a
special warehouse during the weekend due to the chemical property of the ‘Super Apple’. The cost is 5
gold coins per unit per weekend.

2 Short of Stock Penalty: if you cannot meet your customers’ demands for any week, i.e. your stock is
less than your demand, you will be charged by the UN a fixed penalty of 20 gold coins for that week.

3 Return Cost: on the last day of the 52th week, if you still have any stock left, you will need to return
them to the earth with a cost of 10 gold coins per unit. 

# Task

The aim of this project is to minimise the total cost using the Monte Carlo Simulation.

The Coursework Program is done using Matlab and is devided into two seperate files.

Task 2:

Press run
Values for the mean and the variance are displayed, and the histogram of the 500 generated total costs is given.

Task 3:

Press run
Values of the means of  r and y are given.
Figure 1 displays the r values, whilst Figure 2 displays the y values.
