clc;
clear all;
close all;
WC=5;
SP=20;
RC=10;
x=[];
n=500;
R=[];
Y=[];
meantc=[];
for j=1:n;
    TC=zeros(7,7);
    for r=0:6;
        z=r+1;
        for y=0:6;
            m=y+1;
            s=52;
            q=3;
            tc=0;
            for i = 1:s;
                r=rand;
                prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
                x(i) = sum(r >= cumsum([0, prob]));
                x(i)=x(i)-1;
            end 
            for i=1:s-1;
                    q=q-x(i);
                    if q<0;
                        tc=tc+SP;
                    end
                    if q>0;
                        tc=tc+q*WC;
                    end
                    if q<=r;
                        if q<0;
                            q=0;
                        end
                        q=q+y;
                    end  
                end
            for i=s;
                    q=q-x(i);
                    if q<0;
                        tc=tc+SP;
                    end
                    if q>0;
                        tc=tc+q*RC;
                    end
                end
            TC(z,m)=tc;
        end
    end
   [M,I] = min(TC(:));
   [r, y] = ind2sub(size(TC),I);
    R(j)=r-1;
    Y(j)=y-1;
end

bins=7
figure(1);
histogram(R,bins)
figure(2);
histogram(Y,bins)
r=round(mean(R),0)
y=round(mean(Y),0)


