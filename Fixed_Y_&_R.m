%%
clc;
clear all;
close all;

%%
WC = 5;
SP = 20;
RC = 10;
s = 52;
estimates = 500;
x=zeros(estimates,s);
n=500;
TC=[];

%%
for k=1:estimates
    for j=1:n
        y=3;
        r=3;
        q=3;
        tc=0;
        for i = 1:s
                r=rand;
                prob = [0.04, 0.10, 0.26, 0.4, 0.16, 0.02, 0.02];
                x(i) = sum(r >= cumsum([0, prob]));
                x(i)=x(i)-1;
        end 
        for i=1:s-1;
            q=q-x(i);
            if q<0;
                tc=tc+SP;
            end
            if q>0;
                tc=tc+q*WC;
            end
            if q<=r;
                if q<0;
                    q=0;
                end
                q=q+y;
            end  
        end
        for i=s;
            q=q-x(i);
            if q<0;
                tc=tc+SP;
            end
            if q>0;
                tc=tc+q*RC;
            end
        end
        TC(j)=tc;
    end
    TC=TC';
    M(k)=mean(TC);
end
Mean=mean(M)
V=var(M)
%     % nbins=7;
histogram(M)